function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }

function getRandomFLoat() {
    var items = ['right', 'left'];
    var item = items[Math.floor(Math.random()*items.length)];

    return item;
}
function getRandomMargin() {
    var items = ['auto', '0'];
    var item = items[Math.floor(Math.random()*items.length)];

    return item;
}
function getClearBoth() {
    var items = ['both', 'none'];
    var item = items[Math.floor(Math.random()*items.length)];

    return item;
}
  
  $(document).ready(function(){
      $("div").each(function(){
        $(this).css("background-color", getRandomColor);
        $(this).css("clear", getClearBoth);
        var mar = getRandomMargin;
        if (mar == '0') {
            $(this).css("float", getRandomFLoat);
        }
        $(this).css("margin", mar);
      })
  })